import React from 'react';
import DayLabel from './DayLabel/DayLabel';
import styles from './Day.css';
import LineZone from './LineZone/LineZone'

const generateLineZone = (day, areas) => {
    
    return areas.map(area => {
        return (<LineZone key={day.date + "_" + area.area_id} day={day} overlaps={area.overlaps}></LineZone>);
    });



}

const Day = (props) => {

    return (
    <div className={styles.Day}>
        <div className={styles.DateLable}>{props.date.format('Do MMMM YYYY')}</div>
        <DayLabel></DayLabel>
        {generateLineZone(props.day, props.areas)}
        {/* Depending on the ammount of Categories it will create the backgroud of the timeline */}
    </div>)
}


export default Day;