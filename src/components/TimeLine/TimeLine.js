import React, {Component} from 'react';
import Day from './Day/Day'
import AreaBox from './AreaBox/AreaBox'
import styles from './TimeLine.css';
import moment from 'moment';

class TimeLine extends Component {

    today_date = new Date()
    
    state = {
        today_date: moment(),
        // start_date: moment().subtract(7, 'days'),
        // end_date: moment(),
        areas: [
            {            
                area_id: 1,
                area_name: "Cardiology",
                image_url: "",
                overlaps: 1
            },
            {            
                area_id: 2,
                area_name: "Cardiology",
                image_url: "",
                overlaps: 0
            },
            {            
                area_id: 3,
                area_name: "Cardiology",
                image_url: "",
                overlaps: 1
            },

        ],
        days: [
            {
                date:  moment().subtract(1, 'days'),
                events: [
                    {  
                        area_id: 1,
                        name: 'Pedro',
                        start_time: new Date(),
                        end_time: new Date()
                    },
                    {
                        area_id: 2,
                        name: 'Jonny',
                        start_time: new Date(),
                        end_time: new Date()
                    }
               ]
            },
            {
                date:  moment().subtract(2, 'days'),
                events: [
                    {  
                        area_id: 1,
                        name: 'Pedro',
                        start_time: new Date(),
                        end_time: new Date()
                    },
                    {
                        area_id: 2,
                        name: 'Jonny',
                        start_time: new Date(),
                        end_time: new Date()
                    }
               ]
            },
            {
                date:  moment().subtract(3, 'days'),
                events: [
                    {  
                        area_id: 1,
                        name: 'Pedro',
                        start_time: new Date(),
                        end_time: new Date()
                    },
                    {
                        area_id: 2,
                        name: 'Jonny',
                        start_time: new Date(),
                        end_time: new Date()
                    }
               ]
            },
            {
                date:  moment().subtract(4, 'days'),
                events: [
                    {  
                        area_id: 1,
                        name: 'Pedro',
                        start_time: new Date(),
                        end_time: new Date()
                    },
                    {
                        area_id: 2,
                        name: 'Jonny',
                        start_time: new Date(),
                        end_time: new Date()
                    }
               ]
            },
            {
                date:  moment().subtract(5, 'days'),
                events: [
                    {  
                        area_id: 1,
                        name: 'Pedro',
                        start_time: new Date(),
                        end_time: new Date()
                    },
                    {
                        area_id: 2,
                        name: 'Jonny',
                        start_time: new Date(),
                        end_time: new Date()
                    }
               ]
            },
            {
                date:  moment().subtract(6, 'days'),
                events: [
                    {  
                        area_id: 1,
                        name: 'Pedro',
                        start_time: new Date(),
                        end_time: new Date()
                    },
                    {
                        area_id: 2,
                        name: 'Jonny',
                        start_time: new Date(),
                        end_time: new Date()
                    }
               ]
            },
            {
                date:  moment().subtract(7, 'days'),
                events: [
                    {  
                        area_id: 1,
                        name: 'Pedro',
                        start_time: new Date(),
                        end_time: new Date()
                    },
                    {
                        area_id: 2,
                        name: 'Jonny',
                        start_time: new Date(),
                        end_time: new Date()
                    }
               ]
            },
            {
                date:  moment().subtract(7, 'days'),
                events: [
                    {  
                        area_id: 1,
                        name: 'Pedro',
                        start_time: new Date(),
                        end_time: new Date()
                    },
                    {
                        area_id: 2,
                        name: 'Jonny',
                        start_time: new Date(),
                        end_time: new Date()
                    }
               ]
            },
        ]
    };

    
    componentDidMount() {
        //    Gets the difference between the 2 dates.
        //    let DayDifference = Math.trunc(moment.duration(this.state.end_date.diff(this.state.start_date)).asDays());
        //    console.log(duration);
    }

    // Box on the left
    generateAreas() {
        return this.state.areas.map(area => {

            return (<AreaBox key={area.area_id + "_" + area.area_name }>{area}</AreaBox>);
        })
    }

    generateDays() {

        return this.state.days.map(day => {
            return (<Day key={"Day" + day.date} day={day} areas={this.state.areas} date={day.date}></Day>)

        })
    }

    render() {
        return (
            <div className={styles.TimeLine}>
                <div className={styles.LeftPanel}>
                    {this.generateAreas()}
                </div>
                <div className={styles.View}>
                    {this.generateDays()}
                </div>
            </div>
        );
    }
}

export default TimeLine;