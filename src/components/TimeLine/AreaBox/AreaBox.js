import React from 'react';
import styles from './AreaBox.css';


// Check if overlap is bigger than 0.
// get the default value of the Style.
// X2 to that value.
const Day = (props) => {
    let height = 130;

    height = height *  (props.children.overlaps + 1);

    let styleUpdate = {
        height: height + 'px'
    }
    
    return (
    <div className={styles.AreaBox} style={styleUpdate} >
        <h3>{props.children.area_name}</h3>
        
    </div>)
}


export default Day;